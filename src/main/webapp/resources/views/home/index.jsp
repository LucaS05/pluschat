<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<plus-loading ng-show="isLoading"></plus-loading>
<section class="user-list">
	<ul class="list-unstyled">
		<li class="user-list__user" 
				ng-repeat="utente in utenti"
				ng-class="{'user-list__user_active' : isEqualUsername(utente.username)}"> 
			<div class="user__name">{{utente.username}}</div>
			<div class="user__status">
				<i class="fa fa-circle online"></i> online
			</div>
		</li>
	</ul>
</section>
<section class="message-list" scroll-bottom="message-list">
	<ul class="list-unstyled">
		<div ng-if="messaggi.length === 0" class="msg-error_no-message">
			<span class="msg-error__item">Nessun messaggio presente in PlusChat :(</span> Inviane uno adesso!
		</div>
		<li class="message" ng-repeat="msg in messaggi" ng-class="msg.state ? 'message_active' : 'message_nactive'">
			<div class="row">
				<div class="message__user col-md-12">{{msg.username}}</div>
				<div class="message__content col-md-9">
					<div class="message__main">{{msg.message}}</div>
					<div class="message__date">{{msg.date}}</div>
				</div>
			</div>
		</li>
	</ul>
</section>
<section class="send-form ng-scope">
	<plus-mini-loading class="msgspinner" ng-show="msgIsLoading"></plus-mini-loading>
	<send-form send="sendMessage" />
</section>
<script type="text/javascript">
	$(document).ready(function() {
		  var listHeight = $(window).height() - 220;
		  var messageList = $(".message-list");
		  messageList.css("height", listHeight);
		  $(window).resize(function() {
		    var listHeight = $(window).height() - 220;
		    $(".message-list").css("height", listHeight);
		  });
	});
</script>