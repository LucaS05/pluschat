<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html ng-app="PlusChat">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title></title>
	<link rel="stylesheet" href="<c:url value="/resources/lib/bower_components/bootstrap/dist/css/bootstrap.css" />">
	<link rel="stylesheet" href="<c:url value="/resources/lib/bower_components/font-awesome/css/font-awesome.css" />">
	<link rel="stylesheet" href="<c:url value="/resources/css/users.css" />">
	<link rel="stylesheet" href="<c:url value="/resources/css/main.css" />">
</head>
<body>
	<div ui-view>
		<div ng-show="invalidUsername && username" class="msg-error">
			L'username da te scelto: 
			<span class="msg-error__item"><strong>{{username}}</strong></span> 
			non � valido. Esiste gi� un utente online con quel username.
		</div>
		<div class="pc-form pc-form_login">
			<img class="chatlogo" src="<c:url value="/resources/images/logo.png" />" />
			<form name="loginForm" ng-submit="loginUser(loginForm)" class="pc-form" novalidate>
				<label for="usernameText">Username</label>
				<input id="usernameText" type="text" class="form-control pc-form__input" 
					ng-change="validateUser(loginForm)" ng-model="username" ng-minlength="3"
					ng-maxlength="20" name="username" required />
				<div ng-show="loginForm.username.$touched && loginForm.username.$error.minlength"
					class="pc-form__errorlabel">L'username deve essere lungo almeno 3 caratteri</div>
				<div ng-show="loginForm.username.$touched && loginForm.username.$error.maxlength"
					class="pc-form__errorlabel">L'username deve essere massimo di 20 caratteri</div>
				<div ng-show="!loginForm.username.$pristine">
				<div ng-show="loginForm.username.$error.required && loginForm.username.$touched" 
					class="pc-form__errorlabel">L'username � obbligatoria per poter entrare in chat</div>
				</div>
				<p class="placeholder">Inserisci il tuo username</p>
				<input type="submit" class="btn btn-block pcbtn pcbtn_login" ng-disabled="invalidUsername" value="Entra" />
			</form>
		</div>
	</div>
	<script src="<c:url value="/resources/lib/bower_components/jquery/dist/jquery.js" />"></script>
	<script src="<c:url value="/resources/lib/node_modules/stompjs/lib/stomp.js" />"></script>
	<script src="<c:url value="/resources/js/lib/sockjs.min.js" />"></script>
	<script src="<c:url value="/resources/lib/node_modules/spin/dist/spin.js" />"></script>
	<script src="<c:url value="/resources/lib/bower_components/angular/angular.js" />"></script>
	<script src="<c:url value="/resources/lib/bower_components/angular-ui-router/release/angular-ui-router.js" />"></script>
	<script src="<c:url value="/resources/lib/bower_components/angular-resource/angular-resource.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/app.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/common/constants.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/init/initServices.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/init/initControllers.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/users/userService.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/home/homeControllers.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/home/homeDirectives.js" />"></script>
</body>
</html>