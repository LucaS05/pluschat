'use strict';

var homeDirectives = angular.module("home.Directives", ["user.Service"]);

homeDirectives.service("CursorService", function(){
	this.resetPosition = function(txtElement){
		if(txtElement.setSelectionRange){ 
			txtElement.focus(); 
			txtElement.setSelectionRange(0, 0); 
		} else if(txtElement.createTextRange){ 
			var range = txtElement.createTextRange();  
			range.moveStart('character', 0); 
			range.select(); 
		}
	}
});

homeDirectives.directive("scrollBottom", ["$timeout", function($timeout){
	return {
		link: function(scope, element, attr){
			scope.$on("messageRecevied", function (event, args){
				var $elem= $("." + attr.scrollBottom);
				$timeout(function(){
					$elem.scrollTop($elem[0].scrollHeight);
				}, 100);
			});
		}
	}
}]);

homeDirectives.directive("sendForm", ["CursorService", function (cursorService){
	return {
		restrict: "E",
		replace:  true,
		scope : {
			send: "&"
		},
		template: '<form class="form-inline">\
			<textarea class="send-form__text form-control" placeholder="Inserisci il tuo messaggio qui" rows="3" ng-keypress="sendMsg($event)"></textarea>\
			<div class="send-message pull-right">\
			<div class="checkbox enter-checkbox">\
			<label class="enter-checkbox__label" for="enter-checkbox__input">\
			<input id="enter-checkbox__input" class="send-option" ng-click="checkBoxClick()" ng-class="checkboxEnter ? \'active\' : \'nactive\'" ng-model="checkboxEnter" type="checkbox">\
			Utilizza invio per inviare\
			</div>\
			<input ng-show="chnactive" class="btn pcbtn" type="button" value="Invia Messaggio" ng-click="sendMsgClick()">\
			</div>\
			</form>',
			link: function (scope, element) {
				var textArea = element.find("textarea.send-form__text");
				scope.checkboxEnter = false;
				scope.chnactive = true;
				scope.checkBoxClick = function(){
					textArea.focus();
					if(scope.checkboxEnter === true){
						scope.chnactive = false;
					} else {
						scope.chnactive = true;
					}
				};
				scope.sendMsgClick = function(){
					scope.send()(textArea.val());
					textArea.val("");
					cursorService.resetPosition(textArea[0]);
				};
				scope.sendMsg = function($event){
					if($event.keyCode === 13){
						if(scope.checkboxEnter === true){
							scope.send()(textArea.val());
							textArea.val("");
							cursorService.resetPosition(textArea[0]);
						}
					}
				};
			}
	};
}]);

homeDirectives.directive("plusLoading", ["$timeout", function($timeout){
	return {
		restrict: "E",
		replace:  true,
		scope : {
			message: "="
		},
		template: '<div class="overlay">\
						<div id="spinner"></div>\
				  </div>',
			link: function (scope, element) {
				var target = document.getElementById("spinner");
				var opts = {
		                lines: 13,
		                length: 28,
		                width: 14,
		                radius: 42,
		                scale: 1,
		                corners: 1,
		                color: "#fff",
		                opacity: .25,
		                rotate: 0,
		                direction: 1,
		                speed: 1,
		                trail: 60,
		                fps: 20,
		                zIndex: 2e9,
		                className: "spinner",
		                top: "50%",
		                left: "50%",
		                shadow: !1,
		                hwaccel: !1,
		                position: "absolute"
		            };
				var spinner = new Spinner(opts).spin(target);
			}
	};
}]);

homeDirectives.directive("plusMiniLoading", ["$timeout", function($timeout){
	return {
		restrict: "E",
		replace:  true,
		scope : {
			message: "="
		},
		template: '<div class="overlay msgoverlay">\
						<div id="minispinner"></div>\
				  </div>',
			link: function (scope, element) {
				var target = document.getElementById("minispinner");
	            var opts = {
	                    lines: 13,
	                    length: 3,
	                    width: 3,
	                    radius: 6,
	                    scale: 1.5,
	                    corners: 1,
	                    color: "#000",
	                    opacity: .25,
	                    rotate: 0,
	                    direction: 1,
	                    speed: 1,
	                    trail: 56,
	                    fps: 20,
	                    zIndex: 2e9,
	                    className: "spinner",
	                    top: "50%",
	                    left: "50%",
	                    shadow: !1,
	                    hwaccel: !1,
	                    position: "absolute"
	                };
				var spinner = new Spinner(opts).spin(target);
			}
	};
}]);