'use strict';

var homeControllers = angular.module("home.Controllers", ["init.Services", "home.Directives"]);

homeControllers.controller("HomeCtrl", ["$scope", "$state", "$timeout", "InitService", "UserService", function($scope, $state, $timeout, initService, userService){
	if(angular.isDefined(userService.getUsername())){
		var activeUser = userService.getUsername();
		$scope.utenti = [];
		$scope.messaggi = [];
		$scope.isLoading = true;
		$scope.msgIsLoading = false;
		initService.setWindowTitle("PlusChat | Home");
		initService.init("/PlusChat/connect");
		initService.connect(function(frame){
			console.log(frame);
			initService.send("/PlusChat/logged", {}, JSON.stringify({username : userService.getUsername()}));
			initService.subscribe("/topic", function(res) {
				var resBody = JSON.parse(res.body);
				$scope.utenti = resBody.content.utenti;
				if($scope.messaggi.length === 0){
					$scope.messaggi = resBody.content.messaggi;
				}
				$timeout(function(){ 
					$scope.isLoading = false;
				}, 2000);
			});
			initService.subscribe("/topic/message", function(res) {
				var message = JSON.parse(res.body);
				if(message.username === activeUser){
					message["state"] = 1;
				}
				$scope.messaggi.push(message);
				$scope.$broadcast("messageRecevied");
				$scope.msgIsLoading = false;
			});
		}, function(error){
			$state.go("initerror");
			console.log(error);
		});
		$scope.sendMessage = function(msg){
			$scope.msgIsLoading = true;
			initService.send("/PlusChat/message/", {}, JSON.stringify({username : activeUser, message: msg}));
		};
		$scope.isEqualUsername = function(username){
			if(angular.isDefined(username)){
				return activeUser === username;
			}
		}
	} else {
		$state.go("init");
	}
}]);

homeControllers.controller("LogoutCtrl", ["$scope", "$resource", "UserService", function($scope, $resource, userService){
	var UserLogout = $resource("utilities/logout/");
	var res = UserLogout.get({usr : userService.getUsername()}, function(){
		console.log(res);
		//return successCallback(res.content.logged);
	}, function(){
		//return errorCallback();
	});
}]);