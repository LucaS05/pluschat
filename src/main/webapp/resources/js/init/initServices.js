'use strict';

var initServices = angular.module("init.Services", ["ngResource"]);

//var socket = new SockJS("/PlusChat/hello");
//var stompClient = Stomp.over(socket);
//
//var connectCallback = function() {
//console.log("[PlusChat : Status] : Connected!");
//stompClient.subscribe('/topic/greetings', function(greeting){
//	 console.log("[PlusChat : Message] : " + JSON.parse(greeting.body).content);
//});
//sendMessage();
//};
//
//var errorCallback = function(error) {
//// display the error's message header:
//	console.log("[PlusChat : Status] : Error!");
//	console.log(error);
//};
//
//stompClient.connect("guest", "guest", connectCallback, errorCallback);
//
//var sendMessage = function(){
//stompClient.send("/PlusChat/hello", {}, JSON.stringify({ 'name': 'Joe' }));
//};

initServices.service("InitService", ["$rootScope", "$window", function($rootScope, $window){
	var stompClient;
	this.init = function(url) {
		stompClient = Stomp.over(new SockJS(url));
	};
	this.connect = function(successCallback, errorCallback){
		stompClient.connect({}, function(frame) {
			$rootScope.$apply(function(){
				successCallback(frame);
			});
		}, function(error){
			$rootScope.$apply(function(){
				errorCallback(error);
			});
		});
	};
	this.subscribe = function(url, callback){
		stompClient.subscribe(url, function(message){
			$rootScope.$apply(function(){
				callback(message);
			});
		});
	};
	this.send = function(url, headers, object){
		stompClient.send(url, headers, object);
	};
	this.setWindowTitle = function(title){
		if(angular.isDefined(title)){
			$window.document.title = title;
		}
	}
}]);