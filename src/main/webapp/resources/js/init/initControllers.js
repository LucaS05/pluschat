'use strict';

var initControllers = angular.module("init.Controllers", ["user.Service", "init.Services"]);

initControllers.controller("InitCtrl", ["$scope", "$state", "InitService", "UserService", function($scope, $state, initService, userService){
	$scope.invalidUsername = false;
	initService.setWindowTitle("PlusChat | Login");

	$scope.validateUser = function(){
		userService.validate($scope.username, function(valid){
			if(valid === false){
				$scope.invalidUsername = true;
			} else if(valid === true){
				$scope.invalidUsername = false;
			}
		});
	};

	$scope.loginUser = function(form){
		if(form.$valid === true){
			if($scope.invalidUsername === false){
				userService.login($scope.username, function(logged){
					if(logged === true){
						$state.go("home");
					}
				});
			}
		}
	}
}]);