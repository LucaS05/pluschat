'use strict';

var initServices = angular.module("user.Service", ["ngResource"]);

initServices.service("UserService", ["$resource", function($resource){
	var user;
	this.login = function(username, successCallback, errorCallback) {
		var UserLogin = $resource("utilities/loginuser/");
		var res = UserLogin.get({usr : username}, function(){
			user = username;
    		return successCallback(res.content.logged);
    	}, function(){
    		return errorCallback();
    	});
	};
    this.validate = function(username, callback) {
    	if(angular.isDefined(username)){
    		var userNameLen = username.length;
            if(userNameLen >= 3 && userNameLen <= 20){
            	var ValidateUser = $resource("utilities/validateuser/");
            	var res = ValidateUser.get({usr : username}, function(){
            		return callback(res.content.isValid);
            	});
            } else {
                return callback(false);
            }
    	}
    };
    this.getUsername = function(){
    	return user;
    };
}]);