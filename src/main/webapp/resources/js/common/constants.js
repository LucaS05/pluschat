var plusChatConstants = angular.module("PlusChatConstants", []);

plusChatConstants.constant("PlusChatConstants", {
    "FORMS" : {
        "EDIT": "modifica",
        "CREATE": "crea"
    },
    "API_CLIENTE" : {

            "SPEDIZIONI_CLIENTE" : "../api/spdcliente/"
    },
    "API_MAIN" :{
        "CLIENTI" : "../api/clienti",
        "CREA_CLIENTE" : "clienti/create",
        "ATTIVA_CLIENTE" : "clienti/attiva",
        "DISATTIVA_CLIENTE" : "clienti/disattiva",
        "SPEDIZIONI" : "../api/spedizioni",
        "SPEDIZIONE" : "../api/spedizione",
        "CARICA_SPEDIZIONI" : "fileSpedizioni/carica",
        "SALVA_SPEDIZIONI" : "fileSpedizioni/salva"
    },
    "EVENTS" : {
        "FILE_SPEDIZIONI_CARICATO" : "uploadFileSpedizioni"
    }
});