'use strict';

var plusChat = angular.module("PlusChat", ["ui.router", "PlusChatConstants", "init.Controllers", "home.Controllers"]);

plusChat.config(["$stateProvider", "$urlRouterProvider", "PlusChatConstants",
    function($stateProvider, $urlRouterProvider, PlusChatConstants){
		$urlRouterProvider.otherwise("/");
		$stateProvider
            .state("init", {
        		url: "/",
        		controller: "InitCtrl"
            })
            .state("initerror", {
                url: "/error/",
                templateUrl: "partial/initError",
                controller: "InitCtrl"
            })
            .state("home", {
            	url: "/home/",
                templateUrl: "partial/home",
                controller: "HomeCtrl"
            })
            .state("logout", {
            	url: "/logout/",
                templateUrl: "partial/logout",
                controller: "LogoutCtrl"
            });
}]);