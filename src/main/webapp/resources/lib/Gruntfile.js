module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            files: ['Gruntfile.js', 'js/**/*.js'],
            options : {
            	ignores : ['js/lib/**/*.js']
            }
        },
        less: {
            development: {
                options: {
                    compress: false,
                    yuicompress: false,
                    optimization: 0
                },
                files: {
                    "css/main.css": "less/main.less",
                    "css/users.css": "less/users.less"
                }
            }
        },
        watch: {
            scripts: {
                files: ["js/**/*.js","less/**/*.less"],
                tasks: ["less"],
                options: {
                    spawn: false
                }
            }
        }
    });
    grunt.file.setBase('../')
    grunt.registerTask("default", ["watch"]);
};