package to;

import org.springframework.stereotype.Component;

@Component(value="toFactory")
class Factory implements TOFactory{
	
	@Override
	public UserTO getUserTO(){
		return new User();
	}

}
