package to;

public interface UserTO {

	public String getUsername();

	public void setUsername(String username);

	public int getState();

	public void setState(int state);

}