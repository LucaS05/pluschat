package to;

import java.util.Date;

public interface MessageTO{
	int getId();
	void setMessage(String message);
	String getMessage();
	void setUsername(String username);
	String getUsername();
	Date getDate();
	void setDate(Date date);
}
