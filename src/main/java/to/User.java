package to;

import org.springframework.stereotype.Component;

@Component(value="userTO")
class User implements UserTO {

	private String username;
	private int state;
	
	@Override
	public String getUsername() {
		return username;
	}
	
	@Override
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Override
	public int getState() {
		return state;
	}
	
	@Override
	public void setState(int state) {
		this.state = state;
	}
	
	@Override
	public String toString() {
		return "User [username=" + this.username + ", state=" + this.state + "]";
	}
}