package util.repsonse;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component(value="jsonResp")
class JsonResp implements Response{
	private Map<String, Object> content;
	
	JsonResp(){
		this.content = new HashMap<String, Object>();
	}
	
	@Override
	public void addData(String param, Object value){
		this.content.put(param, value);
	}
	
	public Map<String, Object> getContent(){
		return this.content;
	}
	
	@Override
	public void clear(){
		this.content.clear();
	}
}
