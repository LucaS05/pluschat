package controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class PartialsController {
	@RequestMapping(method = RequestMethod.GET)
	public String getLoginView() {
		return "index";
	}
	
	@RequestMapping(value="/partial/home", method = RequestMethod.GET)
	public String getHomeView() {
		return "home/index";
	}
	
	@RequestMapping(value="/partial/initError", method = RequestMethod.GET)
	public String getInitErrorView(ModelMap model) {
		model.addAttribute("title", "Error");
		return "init/error";
	}
	
	@RequestMapping(value="/partial/logout", method = RequestMethod.GET)
	public String getLogoutView(){
		return "user/logout";
	}
}