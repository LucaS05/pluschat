package controllers;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import services.MessageService;
import to.MessageTO;
import users.ChatUsers;
import util.repsonse.Response;

@Controller
@RequestMapping("/utilities")
public class ChatController {

	@Autowired
	@Qualifier("jsonResp") 
	private Response response;
	
	@Autowired
	@Qualifier("userList") 
	private ChatUsers users;
	
	@Autowired
	@Qualifier("messageService")
	private MessageService messageService;
	
	@Autowired
	@Qualifier("messageTO")
	private MessageTO message;

	@RequestMapping(value="/validateuser", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody Response validateUser(@RequestParam("usr") String username){
		this.response.clear();
		if(this.users.isActive(username)){
			this.response.addData("isValid", false);
		} else {
			this.response.addData("isValid", true);
		}
		return response;
	}
	
	@RequestMapping(value="/loginuser", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody ResponseEntity<Response> loginUser(@RequestParam("usr") String username){
		ResponseEntity<Response> resp = null;
		if(this.users.isActive(username)){
			this.response.clear();
			this.response.addData("logged", false);
			resp = new ResponseEntity<Response>(HttpStatus.BAD_REQUEST);
		} else {
			this.users.addUser(username);
			this.response.clear();
			this.response.addData("username", username);
			this.response.addData("logged", true);
			resp = new ResponseEntity<Response>(this.response,HttpStatus.OK);
		}
		return resp;
	}
	
	@MessageMapping("/logged")
	@SendTo("/topic")
	public @ResponseBody Response sendUsers(User user) throws InterruptedException{
	    this.response.addData("utenti", this.users.getUsers(user.getUsername()));
	    this.response.addData("messaggi", this.messageService.getMessages());
	    Thread.sleep(4000);
	    return this.response;
	}
	
	@MessageMapping("/message/")
	@SendTo("/topic/message")
	public @ResponseBody MessageTO sendMessage(Message message) throws InterruptedException{
		this.message.setMessage(message.getMessage());
		this.message.setUsername(message.getUsername());
		this.message.setDate(new Date());
	    this.messageService.saveMessage(this.message);
	    Thread.sleep(4000);
	    return this.message;
	}
	
	@RequestMapping(value="/logout", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody Response logout(@RequestParam("usr") String username) throws InterruptedException{
		this.users.removeUser(username);
		this.response.clear();
	    this.response.addData("logged", false);	    
	    Thread.sleep(4000);
	    return this.response;
	}
}