package controllers;

class Message{
	private String message;
	private String username;

	String getUsername() {
		return username;
	}

	void setUsername(String username) {
		this.username = username;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}

}
