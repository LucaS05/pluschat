package services;

import java.util.List;

import to.MessageTO;

public interface MessageService {
	public void saveMessage(MessageTO message);
	public List<MessageTO> getMessages();
}
