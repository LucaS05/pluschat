package services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import dao.MessageDAO;
import to.MessageTO;

@Component(value="messageService")
class ASMessageService implements MessageService{
	@Autowired
	@Qualifier("messageDAO") 
	private MessageDAO dao;

	@Override
	@Transactional(readOnly = false) 
	public void saveMessage(MessageTO message) {
		this.dao.saveMessage(message);

	}

	@Override
	public List<MessageTO> getMessages() {
		return this.dao.getAllMessages();
	}

}