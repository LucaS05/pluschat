package dao;

import java.util.List;

import to.MessageTO;

public interface MessageDAO {
	public void saveMessage(MessageTO message);
	public List<MessageTO> getAllMessages();
}