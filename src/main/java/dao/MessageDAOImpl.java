package dao;

import java.util.List;

import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Component;

import to.MessageTO;

@Component(value="messageDAO")
class MessageDAOImpl extends HibernateDaoSupport implements MessageDAO{
	
	@Override
	public void saveMessage(MessageTO message){
		if(message != null){
			getHibernateTemplate().save(message);
			getHibernateTemplate().flush();
			getHibernateTemplate().evict(message);
			getHibernateTemplate().load(message, message.getId());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MessageTO> getAllMessages() { 
		return (List<MessageTO>)getHibernateTemplate().find("from Message");
	}
}
