package users;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import to.TOFactory;
import to.UserTO;

@Component(value="userList")
class UserList implements ChatUsers{

	private List<UserTO> mainList;

	private TOFactory tofactory;

	@Autowired
	UserList(TOFactory toFactory){
		this.mainList = new ArrayList<UserTO>();
		this.tofactory = toFactory;
	}

	@Override
	public void addUser(String username){
		if(username != null){
			if(this.findByUsername(username) == null){
				UserTO user = this.tofactory.getUserTO();
				user.setUsername(username);
				user.setState(0);
				this.mainList.add(user);
			}
		}
	}

	@Override
	public void removeUser(String username){
		if(username != null){
			this.mainList.remove(this.findByUsername(username));
		}
	}

	@Override
	public String getUser(String username){
		String res = null;
		if(username != null){
			res = this.findByUsername(username).getUsername();
		}
		return res;
	}

	@Override
	public UserTO[] getUsers(){
		return (UserTO[])this.mainList.toArray(new UserTO[this.mainList.size()]);
	}

	@Override
	public UserTO[] getUsers(String username){
		this.resetUsersState();
		UserTO user = this.findByUsername(username);
		user.setState(1);
		return (UserTO[])this.mainList.toArray(new UserTO[this.mainList.size()]);
	}

	@Override
	public boolean isActive(String username){
		return this.findByUsername(username) != null;
	}

	private void resetUsersState(){
		Iterator<UserTO> iterator = this.mainList.iterator();
		while(iterator.hasNext()) {
			UserTO elem = iterator.next();
			elem.setState(0);
		}
	}

	private UserTO findByUsername(String username){
		UserTO user = null;
		Iterator<UserTO> iterator = this.mainList.iterator();
		while(iterator.hasNext()) {
			UserTO elem = iterator.next();
			String elemUsername = elem.getUsername();
			if(elemUsername.equals(username)) {
				user = elem;
			}
		}
		return user;
	}
}
