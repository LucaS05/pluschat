package users;

import to.UserTO;

public interface ChatUsers {

	void addUser(String user);

	void removeUser(String user);

	String getUser(String user);

	boolean isActive(String user);

	UserTO[] getUsers();

	UserTO[] getUsers(String username);

}