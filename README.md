## Dipendenze ##

**### Dipendenze Backend ###**

Le dipendenze sono gestite tramite Maven e il relativo plugin eclipse m2e. 
Le dipende sono espresse nel file: **/pom.xml**



**### Dipendenze Frontend ###**

Le dipendenze che, per ovvi motivi, non sono state aggiunte al progetto necessitano di essere installate eseguendo nella cartella **/src/main/webapp/resources/lib** i seguenti comandi:

```
npm install
```

```
bower install
```

**## Database ##**

La configurazione per la connessione al database è presente qui:

**/src/main/webapp/resources/database/spring-datasource.xml**

Nella root di questo repository è presente lo script **pluschat.sql** per la creazione del database.