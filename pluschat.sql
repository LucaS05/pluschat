-- phpMyAdmin SQL Dump
-- version 4.3.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dic 21, 2015 alle 10:50
-- Versione del server: 5.6.23
-- PHP Version: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pluschat`
--
CREATE DATABASE IF NOT EXISTS `pluschat` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `pluschat`;

-- --------------------------------------------------------

--
-- Struttura della tabella `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `message`
--

INSERT INTO `message` (`id`, `username`, `message`, `date`) VALUES
(1, 'LucaS05', 'Luca', '2015-12-20 18:55:38'),
(2, 'LucaS05', '\nCiao a tutti!', '2015-12-20 18:55:48'),
(3, 'Mario2', 'Hey', '2015-12-20 18:56:31'),
(4, 'Mario2', '\nCome va?', '2015-12-20 18:56:42'),
(5, 'LucaS05', 'Bene\n', '2015-12-20 18:56:55'),
(6, 'Paolo', 'Ciao a tutti!', '2015-12-20 19:00:39'),
(7, 'Luca', 'Ciao', '2015-12-20 19:00:50'),
(8, 'Paolo', 'Ciao', '2015-12-20 19:01:09'),
(9, 'Luca', 'hello', '2015-12-20 19:01:33'),
(10, 'Barbara', 'Ciao a tutti', '2015-12-20 20:12:31'),
(11, 'Barbara', 'Ciao', '2015-12-20 20:13:56'),
(12, 'Barbara', 'Ciao Mondo', '2015-12-20 20:14:09'),
(13, 'Mario', 'Ciao Barbara', '2015-12-20 20:15:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
